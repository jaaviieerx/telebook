var express  = require('express');
var path = require("path");
var session  = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app      = express();
var port     = process.env.PORT || 8080;
var passport = require('passport');
var flash    = require('connect-flash');
const CLIENT_FOLDER = path.join(__dirname, '/client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');
require('./config/passport')(passport); 
app.use(morgan('dev'));
app.use(cookieParser()); 
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.set('view engine', 'ejs');
app.use(session({
	secret: 'javtelebook',
	resave: true,
	saveUninitialized: true
 } )); 
app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash()); 
require('./app/routes.js')(app, passport); 
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});
app.use(function (err, req, res, next) {
    res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});
app.listen(port);
console.log('The magic happens on port ' + port);
